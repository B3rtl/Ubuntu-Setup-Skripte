# Setup neuer Rechner

## Beschreibung
Dieses Repository enthält Skripte und Tools, die zum Einrichten neuer Systeme verwendet werden.

Bitte die Readme-Dateien in den Unterordnern, mit den Namen der Betriebssysteme ansehen, um weitere Informationen über die Verwendung der Skripte zu erhalten.
