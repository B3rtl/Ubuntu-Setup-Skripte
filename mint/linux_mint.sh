#!/usr/bin/env bash

# Execute itself with sudo if user is not root.
[[ $UID -ne 0 ]] && exec sudo ./"$0"

###
# Set variables
###
export DEBIAN_FRONTEND=noninteractive
DPKG_CFG_FRAGMENT=/etc/dpkg/dpkg.cfg.d/non-interactive

# Linux distro
script_distro='Linux Mint 21'

# Additional packages
packages=(
  # Graphics
  gimp
  inkscape
  pinta
  krita

  # Office
  libreoffice

  # Security
  keepassxc

  # Games
  beneath-a-steel-sky
  billard-gl
  blobby
  blockout2
  flight-of-the-amazon-queen
  freeciv
  frozen-bubble
  gbrainy
  gnome-games
  gweled
  hedgewars
  lincity-ng
  lure-of-the-temptress
  minetest
  openttd
  pingus
  pipewalker
  pokerth
  supertux
  supertuxkart
  teeworlds
  wesnoth
  widelands

  # Gaming platforms
  playonlinux
  steam
  wine-stable
  winetricks

  # System management
  gparted

  # Tools
  f3
  hardinfo
  regionset
  vim

  # Media
  cheese
  vlc

  # Knowledge (incl. programs from the former ubunbu-edu-* meta packages)
  blinken
  calibre
  cantor
  chemtool
  dia
  einstein
  fritzing
  gamine
  gcompris-qt
  goldendict
  inkscape
  kalgebra
  kalzium
  kanagram
  kbruch
  kgeography
  khangman
  kig
  klettres
  kmplot
  kstars
  ktouch
  ktuberling
  kturtle
  kwordquiz
  laby
  lightspeed
  lybniz
  marble
  melting
  parley
  pencil2d
  ri-li
  rocs
  step
  tuxmath
  tuxpaint
  tuxtype
  yorick

  # Language support
  openoffice.org-hyphenation
  language-pack-gnome-ar
  language-pack-gnome-fa
  language-pack-gnome-ru
  language-pack-gnome-uk
)

###
# Functions
###

# $*: message to echo.
e() {
  printf "\e[31m>>>\e[0m %s\n" "$*"
}

# It's a pain to stop execution (CTRL+C, kill) with all the Python applications
# so we catch SIGINT and SIGTERM and exit immediately.
handle_signal() {
  e "Ausführung abgebrochen! Skript wird beendet."
  exit 1
}

# Always executed when exiting the shell, regardless of the reason.
handle_exit() {
  e "Finale Aufräumarbeiten werden durchgeführt …"
  rm -f "$DPKG_CFG_FRAGMENT"
  rm -f /etc/apt/apt.conf.d/01aptproxy 
}

trap handle_signal SIGINT SIGTERM
trap handle_exit EXIT

pkg() {
  apt install "$@" --yes --quiet
}

###
# Greeting
###
echo "\
#####################################
# Computertruhe-Installationsskript #
#####################################

###
# Maintainer: Computertruhe e. V.
# Website:    https://computertruhe.de/
# Version:    2.0.0
# Repo:       https://codeberg.org/Computertruhe/Setup-Skripte
# Distro:     ${script_distro}
###
"
e "Starte initiales Setup für Rechner mit frisch installiertem '${script_distro}' …"

# Use APT proxy if available.
ping -c 1 apt-proxy
if [ "$?" -eq "0" ]; then
	e "Nutze APT-Proxy."
	echo 'Acquire::https::Proxy "http://apt-proxy:3142";' > /etc/apt/apt.conf.d/01aptproxy
	echo 'Acquire::http::Proxy "http://apt-proxy:3142";' >> /etc/apt/apt.conf.d/01aptproxy
else
	e "Kein APT-Proxy gefunden."
fi
echo 'Dpkg::Options "--force-unsafe-io";' >> /etc/apt/apt.conf.d/01aptproxy


###
# Automatic installation
###
# Place temporary dpkg configurations to ensure non-interactive upgrade.
cat <<'DPKG' >"$DPKG_CFG_FRAGMENT"
force-confold
force-confdef
DPKG

e "Besten Spiegelserver für '${script_distro}' auswählen …"
mint-switch-to-local-mirror

e "Paketquellen aktualisieren …"
apt update --yes --quiet

e "System aktualisieren (apt) …"
apt full-upgrade --yes --quiet

e "System aktualisieren (mintupdate-cli) …"
mintupdate-cli --yes upgrade

e "Proprietäre Treiber installieren (sofern verfügbar) …"
ubuntu-drivers autoinstall

e "Multimedia-Codecs installieren …"
pkg mint-meta-codecs

e "Zusätzliche Software installieren …"
pkg "${packages[@]}"

e "Sprachunterstützung vervollständigen …"
pkg $(check-language-support)
e "Arabische Schrift"
pkg fonts-arabeyes
e "Kyrillische Schrift"
pkg xfonts-cyrillic

# These packages have to be installed after "$(check-language-support)".
pkg hunspell-de-at hunspell-de-ch hunspell-de-de hunspell-uk hunspell-ru

e "Unnötige Pakete entfernen und Cache bereinigen …"
apt clean --yes --quiet
apt autoremove --yes --quiet

e "Initiales Setup beendet."
