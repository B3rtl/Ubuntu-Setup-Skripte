#!/bin/bash

if [ -z "$(command -v dialog)" ]; then
	echo "dialog fehlt! Installiere dialog..."
	sudo apt update
	sudo apt install -y dialog
fi


while true;
do
  dialog --clear
  # select filename using dialog
  IMG=$(dialog --title "Image-Datei wählen. Mit *Leertaste* selektieren" --stdout --fselect ./ 14 88)

  GG=$(lsblk -r -p -n -o NAME,TYPE |grep disk | cut -f1 -d' ')
  echo $GG
  Devs=()
  for dev in $GG; do
    Devs+=($dev)
    Devs+=("")
  done

  DEV=`dialog --menu "Festplattengerät wählen" 0 0 0 \
     "${Devs[@]}" 3>&1 1>&2 2>&3`
  dialog --clear

  dialog --yesno "Bestätigen Sie Ihre Auswahl: $DEV <- $IMG" 0 0
  # Get exit status
  # 0 means user hit [yes] button.
  # 1 means user hit [no] button.
  # 255 means user hit [Esc] key. 
  response=$?
  case $response in
   0) echo "Bestätigt"; break;;
   1) echo "Sicherheitshalber beendet"; exit 1;;
   255) echo "[ESC], Sicherheitshalber beendet"; exit 1;;
  esac
done


#---------

if [ -z "$(command -v growpart)" ]; then
	echo "growpart fehlt! Installiere cloud-guest-utils..."
	sudo apt update
	sudo apt install -y cloud-guest-utils
fi

if [ -z "$IMG" ] ; then
echo "Image-File angeben!"
exit 1
fi

if [ -z "$DEV" ] ; then
echo "HDD dev angeben! z. B. /dev/hdc"
exit 1
fi

echo "Warte 2 Sekunden vor dem löschen, abbrechen mit strg + c..."
sleep 2
echo "Achtung - vorbei, ich mache jetzt alles platt auf" "${DEV}"

case "$IMG" in
  *.bz2)
    sudo dd if="$IMG" bs=1M status=progress | bzip2 -d > "$DEV"
    ;;
  *)
    sudo dd if="$IMG" of=$DEV bs=1M status=progress
    ;;
esac
sync
sudo growpart $DEV 3
sudo mount  ${DEV}3 /mnt
sudo btrfs filesystem resize max /mnt
sudo umount /mnt
sync
